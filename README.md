# CMake/CI/TDD MCU example project

This project is an example [CMake](https://cmake.org) project based on [Plane for students](https://gitlab.cern.ch/mro/support/training/plane) hardware.

The main goal is to show how CMake/CppUnit/CI can be leveraged to build and work on
very constrained environments (here an _ATmega328p_ MCU).

We will describe step-by-step the approach to build and construct software on
top of very constrained environment, here an *ATmega328p* MCU.

## The project

Using the [Plane](https://gitlab.cern.ch/mro/support/training/plane) hardware:\
<img src="./docs/plane.jpg" align="center" width="50%"/>

This project's aims to:
- Develop on a small size MCU (8-bit) : [ATMega328P](https://www.microchip.com/en-us/product/atmega328p)
- Using a dedicated toolchain : [avr-gcc](https://www.microchip.com/en-us/tools-resources/develop/microchip-studio/gcc-compilers)
along with [avr-libc](https://www.nongnu.org/avr-libc/)
- Use an uni-directional peripheral (hard to test) : [W2812B](https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf) LEDs
- Use a bidirectional peripheral (I2C/TWI) : [MPU6050](https://cdn.sparkfun.com/datasheets/Sensors/Accelerometers/RM-MPU-6000A.pdf) accelerometer
- Implement a (really) simple application :
    - Blink "big" LEDs 1sec every 4seconds
    - Light "large" LEDs according to plane attitude / acceleration.

## Requirements

The only requirements to get through this training are:
- A Unix/Linux friendly machine (all this training was prepared on Linux, no special efforts were made to support any other env).
- A working [docker](https://www.docker.com/) or [podman](https://podman.io/) environment.
- A [BE/CEM/MRO plane](https://gitlab.cern.ch/mro/support/training/plane) hardware,
or a [Maker Nano](https://www.cytron.io/p-maker-nano-simplifying-arduino-for-projects) board.

## Getting Started

This project introduces the following topics:
1. [Docker and Development Environment](./docs/Docker.md)
    - optional [IDE/VSCode integration](./docs/IDE.md)
2. [CMake and Rules](./docs/CMake.md)
3. [Tests and Validation](./docs/Tests.md)
4. [Code Quality](./docs/Linting.md)
5. [Continuous Integration](./docs/CI.md)

## What this project is **NOT**

This project is *baremetal* on purpose, to demonstrate how to get started from
scratch, we could have used an external SDK (ex: Arduino) but that was **not** the point.

This project is **not** intended to replace the
[plane](https://gitlab.cern.ch/mro/support/training/plane) trainee project,
it is **not** designed for beginners and focuses mainly on the approach,
the BE/CEM/MRO *plane* is used only as an example *MCU*.

This project is **not** an example of software design, no special efforts were
made to design and architecture its code, the idea is to focus on tools and
different approaches, not to produce any-kind of production software.

Decisions taken and choices made were to get straight to the point, this is **not**
a definitive guide on how to develop on MCUs, different choices and techs could
have been used (the ones chosen here were mainly for the sake of simplicity).
