# CMake

## Introduction

In this document we'll introduce step-by-step how to use [CMake](https://cmake.org)
to cross-compile a project using a **custom toolchain** and prepare it
**CI/CD** integration.

For a more in-depths CMake introduction and training, please refer to the [dedicated training](https://slides.com/sylvainfargier/makefile#/38).

## Basic setup

To get started a minimal `CMakeLists.txt` file is required, with at least the following:
```cmake
cmake_minimum_required(VERSION 3.12)

project(plane
    LANGUAGES C CXX ASM
    HOMEPAGE_URL "https://gitlab.cern.ch/mro/support/training/cmake-ci-tdd-example"
    DESCRIPTION "CMake/CI/TDD example project"
    VERSION "1.0.0")

use_cxx(11)
use_c(11)

add_executable(plane "src/main.cpp")
```

Supposing that you have an existing `src/main.cpp` file, this minimalist `CMakeLists.txt`
will compile it and generate a `plane` executable.

It can be tested with the following commands:
```bash
# Always prefer "out-of-source" builds
mkdir build && cd build
cmake .. && make
```

✋ This should have compiled the `plane` executable with a native toolchain
(default CMake behavior).

👉 To *cross-compile* with a custom toolchain a **toolchain-file** is required,
let's get into this !

## Toolchain file

Most compilers are [already supported](https://gitlab.kitware.com/cmake/cmake/-/tree/master/Modules/Compiler)
by CMake and the appropriate configuration is automatically selected by CMake depending
on `CMAKE_<lang>_COMPILER` value, ex:
```bash
# To cross-compile using IAR
cmake -DCMAKE_C_COMPILER=iccarm
```

Our target MCU being an 8-bit ATMega328P micro-controller, we decided to pick
a toolchain that was not already supported by CMake :
[avr-gcc](https://www.microchip.com/en-us/tools-resources/develop/microchip-studio/gcc-compilers)
along with [avr-libc](https://www.nongnu.org/avr-libc/).

So a **toolchain-file** is required, for more details about those files, please see
[cmake-toolchains.7 documentation](https://cmake.org/cmake/help/latest/manual/cmake-toolchains.7.html).

Our toolchain file (located here: [avr-toolchain.cmake](../avr-toolchain.cmake))
looks for tools and binaries that are part of this toolchain and then configures
`CMAKE_*` dedicated variables:
```cmake
find_program(AVRCPP avr-g++ REQUIRED)
find_program(AVRC avr-gcc REQUIRED)
find_program(AVRSTRIP avr-strip REQUIRED)
find_program(OBJCOPY avr-objcopy REQUIRED)
find_program(OBJDUMP avr-objdump REQUIRED)
find_program(AVRSIZE avr-size REQUIRED)

set(CMAKE_SYSTEM_NAME  Generic)
set(CMAKE_C_COMPILER   ${AVRC})
set(CMAKE_CXX_COMPILER ${AVRCPP})
set(CMAKE_ASM_COMPILER   ${AVRC})

set(CMAKE_C_FLAGS_INIT "${CMAKE_C_FLAGS_INIT} -Os -gstabs -mmcu=${MCU}  -DF_CPU=${F_CPU} -DBAUD=${BAUD}")
```

One of the specificities of our platform is that we target a very specific MCU and
environment, so we decided to add variables with default values that matches
our target:
- `MCU` : target micro-controller, here "atmega328p".
- `F_CPU` : main clock frequency, here "16000000".
- `BAUD`, `PROG_TYPE`, `PROG_DEVICE` : everything needed to flash and hook the board using [avrdude](https://github.com/avrdudes/avrdude).

In addition to this, two functions are also provided when this toolchain-file is
used:
- `avr_prepare_executable` : to make some post-build transforms on generated binaries.
- `avr_flash_target` : to use [avrdude](https://github.com/avrdudes/avrdude) to flash binaries on boards.

✋ Those functions will be known by CMake only when the toolchain-file is loaded,
so their invocation must be guarded, ex:
```cmake
if(CMAKE_C_COMPILER_TARGET STREQUAL "avr")
    avr_prepare_executable(plane)
    add_flash_target(flash plane)
endif()
```

👉 To use our toolchain-file ([avr-toolchain.cmake](../avr-toolchain.cmake)) and
cross-compile our first binary :
```bash
mkdir build-avr && cd build-avr
cmake .. -DCMAKE_TOOLCHAIN_FILE=./avr-toolchain.cmake
make

# And then to flash our device:
make flash
```

## Additional rules and options

Having a simple operational *CMake* project, we will now inject **rules and guidelines**
that were definied by your team.

The main purpose is to provide the same level of integration and same set of
rules on all our projects, the minimal set of rules being:
- `style` : invoke [clang-format](https://clang.llvm.org/docs/ClangFormatStyleOptions.html) to apply styling.
- `test` / `test-verbose` : run unit-tests, generally based on [CppUnit](https://freedesktop.org/wiki/Software/cppunit/) for C++ projects.
    - only available when tests are enabled using `-DTESTS=ON` option when invoking CMake.
- `lint` : invoke detected linters [CppCheck](https://cppcheck.sourceforge.io/), [Clang Checkers](https://clang.llvm.org/docs/analyzer/checkers.html), [pedantic/walled GCC](https://gcc.gnu.org/onlinedocs/gcc/Warning-Options.html) (others may be added...)
    - `lint-badge` : generate a lint badge
    - `git-badge` : generate a version badge
- `lcov` : generate code-coverage report using [lcov](https://github.com/linux-test-project/lcov)
    - only available when code-coverage is enabled using `-DCOVERAGE=ON` option when invoking CMake.
- `doc` : generate [Doxygen](https://www.doxygen.nl) documentation
    - only available when enabled

✋ This **is not** an **exhaustive list** of tools to use, those are examples,
**other** better/faster/nicer **tools exist**,
also those may change (depending on retrospectives and new trending tools injection 😛).

🥳 And that's it, all our projects can easily be compiled/tested/linted/styled... with
same rules and tools.

👉 This is rather important for developers to jump from one project to another,
and it is **vital** for [CI integration](./CI.md), minimizing the amount of script to hook-in
builders.

### Hooking our tools

Those rules and options are hosted on a [dedicated repository](https://gitlab.cern.ch/mro/common/cpp/cmake) (shared with all our
projects).

This repository is hooked on this project using a [Git submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules) :
```bash
git submodule add https://gitlab.cern.ch/mro/common/cpp/cmake.git cmake

cp cmake/Init.cmake .

# Adds `include(./Init.cmake)` below `cmake_minimum_required` line
grep -q Init.cmake CMakeLists.txt || sed -i 's#\(cmake_minimum_required.*\)#\1\n\ninclude(./Init.cmake)#' CMakeLists.txt
```

✋ The small `Init.cmake` script that you have to copy in your workspace will
initialize the submodule on freshly cloned repositories, so no specific Git
configuration or commands are required.

### Project specific rules and options

Some **project specific options** may be useful, in this project the following
options were added:
- `-DUSE_AVR_TOOLCHAIN=True` : do not use the native compiler, compile for the target.
    - this is an alternative to `-DCMAKE_TOOLCHAIN_FILE=avr-toolchain.cmake`, just simplifying things.

To list those options (and review their current state):
```bash
# From your build directory
ccmake ..
```

Some **project specific rules** may also be added, depending on the project's need,
on this sample project the following were added:
- `flash` : flash application on device using [avrdude](https://github.com/avrdudes/avrdude).
- `flash-integ_<name>` : flash integration test on device.

Those rules may be listed using:
```bash
# From your build directory
make help
```
