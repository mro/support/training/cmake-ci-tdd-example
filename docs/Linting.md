# Linting

Linting is mainly running static-analysis tools on the code-base, trying to
detect and fix miss-constructs as soon as possible.

To make our tools consistent and fasten developments, all our projects provide a
*lint* rule that calls detected linters one by one:
```bash
x-builder make -C build lint
```

👉 please refer to:
- [CMake.md](./CMake.md) to understand what `lint` rule does.
- [Docker.md](./Docker.md) to understand where those tools are running
and what is `x-builder`.
- [CI.md](./CI.md) to understand how those rules are enforced.

# Code-Style

Somehow related to Linting, enforcing an homogenous code-style on each project
is really important.

To make our tools consistent and fasten developments, all our projects provide a
*style* rule that calls code-styling tool:
```bash
x-builder make -C build style
```

👉 please refer to:
- [CMake.md](./CMake.md) to understand what `style` rule does.
- [Docker.md](./Docker.md) to understand where those tools are running
and what is `x-builder`.
- [CI.md](./CI.md) to understand how those rules are enforced.
