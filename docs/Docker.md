
# Docker development environement

## Introduction

In this document we'll introduce step-by-step **docker based build environments**.

Such environments are generally used for [CI](./CI.md) and to prepare production builds,
the idea here is to use the same environment for development.

### Requirements

Only dev-machine requirement is a working **Docker** (or **Podman**) environment:
```bash
apt-get install -y docker.io
# or
apt-get install -y podman
```

### Naive approach : basic build

**Note:** do not execute those steps, this is an example.

On a *debian* machine, install the following tools:
```bash
apt-get update
apt-get install -y gcc-avr avr-libc avrdude cmake
```

To **build** the project for target MCU:
```bash
mkdir build; cd build
cmake .. -DUSE_AVR_TOOLCHAIN=True && make
```

To run various project tasks:
```bash
# Show available targets (CMake built-in)
make help

# Run unit-tests
make test

# Apply code-styling
make style

# Run code linter(s)
make lint

# Flash actual target:
make flash
```

✋ The result of those commands will depend on tools available on your host, ex:
*style* requires [clang-format](https://clang.llvm.org/docs/ClangFormat.html),
*lint* can use [gcc](https://gcc.gnu.org/), [clang](https://clang.llvm.org/), [CppCheck](https://cppcheck.sourceforge.io/).

So depending on your host setup, version and availability of those tools, results may differ from one machine to another.

💡To work this arround it is recommended to use a **docker build environment**, preparing a container (virtual-machine) with the exact set of tools required to build this project.

Benefits in doing so are (not exhaustive):
- Projects comes with their toolsets -> jumping from one topic to another is transparent (no more old-tools trashing dev-machines).
- CI and developpers use the same set of tools -> no more headaches trying to understand what's wrong with CI.
- Projects may be kept alive by CI, automatically updating both dependencies and build-environment itself, being notified about breaking-changes as it occurs.
- *Heavy* environments (read Eclipse, Xilinx/Vivado and other golgoth) tend to spawn a lot of "helpers" and scripts, messing with your machine -> containers will restrain this and allow to kill/clean everything at once.

👉 We'll get to this step-by-step.

## Running docker as a development environment


To run docker as a build environement, the following command may be used (replace `docker` by `podman` in all steps if you're using podman):
```bash
# From the project's directory:
docker run -v "$PWD:$PWD" -w "$PWD" --rm -it debian:stable-slim /bin/bash
```

✋ What's happening there:
- we're starting a `debian:stable-slim` container, eventually pulling it from docker.io (depending on your configuration).
- the container is made *volatile* (`--rm` option), any changes made to the container itself are lost when leaving.
- the container is made *interactive* and hooked to current *terminal* (`-i -t` options) running a `bash` shell.
- current directory is *mounted* in the container, using identical path inside and outside and moving into it (`-v $PWD:$PWD -w $PWD` options).

From this shell you could run the steps described in the *Naive approach* and compile the project, but that would be tedious.

**Note:** using identical paths outside/inside the container is quite important for IDE's integration,
especially when using linters and language-servers (we'll get into that later).

👉 Let's build a container suited for this project.

## Building a container

To prepare the development environement:
```bash
docker build -t cmake-plane-builder:latest -f Dockerfile .
```

✋ What's happening here:
- We're telling docker to build a container that will be named `cmake-plane-builder` with tag `latest`.
- Our new container will be built using `Dockerfile` (we'll get into that one).
- Current directory (the `.`) will be used as build context (`COPY` and `ADD` commands from `Dockerfile` will refer to that directory).

👉 Now let's have a look into this **[Dockerfile](../Dockerfile)**, nothing crazy there:
- start `FROM` a reference image (preferably a LTS distro), here `debian:stable-slim`.
- `RUN` a bunch of commands installing your tools and preparing the container.

🎉 and voila !

**Note:** here's a complete reference here: [Dockerfile syntax](https://docs.docker.com/reference/dockerfile).

## Running the dev-environment

Now that we've built our image, let's step into it:
```bash
# From the project's directory:
docker run -v "$PWD:$PWD" -w "$PWD" --rm -it cmake-plane-builder:latest /bin/bash
```

We can now configure and compile our project:
```bash
mkdir build; cd build
cmake .. -DUSE_AVR_TOOLCHAIN=True && make
```

✋ With those few steps we've described on how to build and prepare a build environment for our project,
any participant of this project can do the same and work in an (almost) identical environment,
but typing the command above and having to remember the image's name is error-prone.

👉 To make this build environement **reproducible** and identical for all developers,
not having to prepare it on your own machine, a [CI](./CI.md) job should be used,
this will be seen in the [CI](./CI.md) topic

👉 If we are to use the same pattern on every project we'd rather make it user-friendly ! that's what *x-builder* does !

## Docker development tools

To ease the process described above a **lightweight** script (remember: always *K.I.S.S.*)
is available: [x-builder](https://gitlab.cern.ch/mro/common/tools/x-builder)

The purpose of this script is to check for a file and to spawn the command we've seend above (eventually doing much more like hooking on your X11, but that's more advanced topic).

Once [x-builder](https://gitlab.cern.ch/mro/common/tools/x-builder) is installed and made available in your path one only requirement is a `.docker-builder` or `.docker-builder.sh` configuration file.

For this project we'll use a [.docker-builder.sh](../.docker-builder.sh) file (to add additional options).

Now to enter your environment, simply run:
```bash
# From the project's directory:
x-builder

# Or even shorter: spawn a container, run a command then step-out of it:
x-builder make -C build
```

🥳 that's it ! you now have all the tools to jump into any projects, even if you haven't been
working on it for months (years ?), simply jumping in its container,
focusing on your actual task, and making it to production in a few steps ✨

We'll see in the next chapters that pipelines and CI will even fasten this up. But let's wait until we get there 😝.

Additional information on how to hook your IDE is available [there](./IDE.md).

## Side words

This section is **not part of the training**, just some side help to setup docker.s

Docker requires a bit of setup to run in usermode (your_user == root in the container).

On your Linux distribution do create the following files:
- in **/etc/docker/daemon.json** :
```json
{"userns-remap":"my_user"}
```
- in **/etc/subuid** :
```
my_user:my_user_id:65536
```
- in **/etc/subgid** :
```
my_user:my_user_gid:65536
```

To find your user id/gid, use the command: `id`

Then restart docker:
```bash
sudo systemctl restart docker
```

To ensure this has been setup properly, files belonging to you in the workspace
should belong to `root:root` in the container, and any file created in the container
should belong to your user outside.

This setup is not needed with podman.