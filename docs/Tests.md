# Test Driven Development

## Introduction

In this document we'll introduce different **test and validation** approachs.

Considering that test and validation is split in three categories (there can be
more, as mentionned in all documents this presentation is opinionated), we'll
focus mainly on component level testing: **unit-tests** and **integration-tests**,

👉 Here's a brief summary on the *three categories* of tests, each

### Unit-tests / White-box testing

👉 Testing actual code and implementation, focusing on component's logic.

External dependencies **must** be stubbed and/or created/destroyed for each test,
**no setup** and no **external dependencies** should be required.

### Integration-tests / Grey-box testing

👉 Testing how things work together, removing some stubs.

We may **stub only part** of the external environement,
**focusing on the integration** of a specific external component.

In-between unit and functional tests, we're testing communication between two
pieces of software, or hardware integration, eventually relying on external pieces
and **requiring some setup**.

### Functional-tests / Black-box testing

👉 Device or product level test, we're testing the **complete product or system**, there may not
be any stubs, unless to test conditions that can't be reached with regular devices
(error and failure cases).

✋ Functional-tests (as well as production tests and hardware validation) were
left aside from this project, those would have been mandatory if this project was
meant to be industrialized, a totally different approach and set of tools would
have been used for those (out of the scope of this training).

## Tests Implementation

### Preparing the application

In most C/C++ [TDD](https://en.wikipedia.org/wiki/Test-driven_development) oriented
projects you'll find the following pattern:
- a minimalist `main` file (that can't really be tested)
- an internal static library that contains all the application's code.
    - it will be linked with the `main` to produce the final binary.
    - and linked with unit-tests.

Here's how this is achieved in this project, in [src/CMakeLists.txt](../src/CMakeLists.txt) :
```cmake
# Collect files to be compiled
file(GLOB_RECURSE SRC "*.cpp" "*.c")
# Reject main from "SRC"
list(FILTER SRC EXCLUDE REGEX "main[.]cpp")
# Reject "stub" files
list(FILTER SRC EXCLUDE REGEX "stub/")

# Create a static internal library (never installed)
add_library(planelib STATIC ${SRC})
# Making this public will transfer it to anyone using this target/lib
target_include_directories(planelib PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

# Add final binary
add_executable(plane "main.cpp")
# Link it with internal lib
target_link_libraries(plane planelib)

if(CMAKE_C_COMPILER_TARGET STREQUAL "avr")
    # if target is "AVR", binary requires some preparation (see CMake.md)
    avr_prepare_executable(plane)
    add_flash_target(flash plane)
else()
    # if we're not compiling for target, add stubs, link internal lib with stubs
    file(GLOB_RECURSE STUB_SRC "stub/*.cpp")
    add_library(stub STATIC ${STUB_SRC})
    target_link_libraries(planelib stub)

    target_include_directories(planelib PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/stub)
endif()
```

✋ Using *globbing* in *CMakeLists.txt* files is a bit controversed in CMake
community, main advantage is that you don't have to edit *CMakeLists.txt*
to add/remove files, main drawback is that you have to explicitly invoke
`cmake` when adding/removing files (choice is yours, not the point for this training).

### Integration tests

Since this project started *baremetal* first tests implemented were
**integration tests**, to test and validate drivers and basic operating-system
for the platform.

Those are implemented as self-contained applications, in [tests/integ_*.cpp](https://gitlab.cern.ch/mro/support/training/cmake-ci-tdd-example/-/tree/master/tests) files.

Three different parts were implemented/tested using this technique:
- [integ_system.cpp](../tests/integ_system.cpp) : system tests (clocks and delays).
- [integ_leds.cpp](../tests/integ_leds.cpp) : led driver validation.
- [integ_gyro.cpp](../tests/integ_gyro.cpp) : gyro/accelerometer validation.

All those tests are:
- focusing on a **single feature/peripheral**.
- **self-descriptive**, implemented in a linear style (step by step), asking user to do actions/prepare the test.
- **require** some **hardware** but with a very basic setup.

The **benefits** in saving those files and spending time making it re-usable :
- Ensure software **integrates** with external component (here the hardware).
- Easily test external component integration (here the hardware) and quickly **identify defects** root-cause.
- Provide a **base/reference software** to communicate with each peripheral.

The **drawbacks** :
- Those tests require hardware and setup, it'll be **hard to automate** 👉 those never replaces unit-tests.
- **Writing and maintaining** is **tedious**, but after a couple years you may not even
remember what a `FABLed<WS2812B, 3, PORT::B, PB4>` is 👉 those tests demonstrate
how hardware is supposed to work and gives simple functional examples.

👉 Those tests never replaces unit-tests, the purpose is really to remove stubs
ensuring a specific part actually works.

#### Compiling integration tests

In [tests/CMakeLists.txt](../tests/CMakeLists.txt) :
```cmake
# Collect all "integration" tests
file(GLOB INTEG_SRC "integ_*.cpp")
foreach(SRC IN LISTS INTEG_SRC)
    # extract the filename, no extensions or path
    get_filename_component(INTEG_NAME "${SRC}" NAME_WE)

    # generate an executable with the same name than source-file
    add_executable(${INTEG_NAME} ${SRC})
    # link it with static internal library
    target_link_libraries(${INTEG_NAME} planelib)

    if(CMAKE_C_COMPILER_TARGET STREQUAL "avr")
        # prepare binary for target and add flashing rule (see CMake.md)
        avr_prepare_executable(${INTEG_NAME})
        add_flash_target(flash-${INTEG_NAME} ${INTEG_NAME})
    endif()
endforeach()
```


### Unit tests

Once the *system* is functional and basic drivers are working (TWI/I2C ...),
the idea is to stub those external dependencies and **validate our software**,
focusing on the code we write.

Those tests are implemented using [CppUnit](https://freedesktop.org/wiki/Software/cppunit/) in [tests/test_*.cpp](https://gitlab.cern.ch/mro/support/training/cmake-ci-tdd-example/-/tree/master/tests) files, those files are
compiled together and linked with the main application.

✋ It is rather important to decide at which level those stubs should be placed,
the difficulty is to **not stub your own code**, the more of your stack you test the better.

👉 Best is to stub only external dependencies (when feasible), replacing external
libraries (or IPs in baremetal) with custom tailored implementation, you can find our [avr-libc](https://www.nongnu.org/avr-libc/) stub in [src/stub/avr](../src/stub).

#### I2C/TWI stubbing

The media and peripheral we chose is on *I2C*, to unit-test the complete stack the best is to **stub
MCU's IPs** themselves.

Depending on the IP stubbing may be as simple as replacing registers with RAM data, adding a thread
to watch and animate those registers, emulating hardware, this is the path we chose for LEDs and serial-port
(from [src/stub/avr/io.h](../src/stub/avr/io.h)):
```cpp
extern volatile uint8_t PORTD;
extern volatile uint8_t DDRD;
extern volatile uint8_t PIND;
```

✋ But this does not give much flexibility and doesn't work with clear-on-write registers,
luckily our *I2C/TWI* IP works exactly that way 😅 :
```cpp
// Writing 1 in TWINT clears it
TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTA); // Send START
// Now we wait for TWINT to be raised by MCU
//   This code can hardly be stubbed with simple registers stubs
while (!(TWCR & (1 << TWINT)))
    ;
```

Now let's think a bit 🤔, we can either:
- give up and stub at a higher level, not unit-testing our TWI driver ([src/TWI.hpp](../src/TWI.hpp))
- dig into the test-system's direction, example: use protected memory with traps playing with `mmap` and ldscript (that's a bit overkill, but already had similar cases, having to stub syscalls).
- dig into the language's direction, we decided to use C++ and a register doesn't have to be a plain value !

Our 8-bit control register is replaced with an object that can mimick the IP(from [src/stub/util/twi.h](../src/stub/util/twi.h)):
```cpp
class twcr_t
{
public:
    twcr_t &operator=(uint8_t value);

    bool operator&(uint8_t value) const { return value & m_value; }

    inline uint8_t &value() { return m_value; }

protected:
    uint8_t m_value;
};
extern twcr_t TWCR;
```

In the end we don't even have to use threads or complicated mechanisms,
all the ✨ magic ✨ happens in this `operator =`, stubbing our entire I2C/TWI bus
in ~160 lines, see [src/stub/util/twi-stub.hpp](../src/stub/util/twi-stub.hpp)
and [src/stub/util/twi.cpp](../src/stub/util/twi.cpp).

👉 Now complete devices can be stubbed on the other side of the bus
(see [src/stub/util/MPU6050Stub.hpp](../src/stub/util/MPU6050Stub.hpp))
implemented and unit-tested, not having to even have an actual hardware.

This stub is used to unit-test both our [MPU6050](../src/twi/MPU6050.hpp) and
[TWI](../src/TWI.hpp) implementations in [tests/test_MPU6050.cpp](../tests/test_MPU6050.cpp)
as well as our final application in [tests/test_Plane.cpp](../tests/test_Plane.cpp)
.
#### Compiling unit-tests

👉 [CppUnit](https://freedesktop.org/wiki/Software/cppunit/) requires a minimal setup,
this is achieved by injecting [tests/test_main.cpp](../tests/test_main.cpp) file.

This file is the same on all our projects and simply dragged-over (this could have been automated
with cmake submodule, but remember KISS, automating too much makes it worse, the border is tight 😝).

In [tests/CMakeLists.txt](../tests/CMakeLists.txt) :
```cmake
# compiling unit-tests is an option (not available on target, since we use CppUnit that requires STL)
if(TESTS)
    # collect test files
    file(GLOB TEST_SRC "test_*.cpp")

    # create a "test_all" exe
    add_executable(test_all ${TEST_SRC})
    # link it with static internal library and cppunit
    target_link_libraries(test_all planelib cppunit)

    # declare it as a test to CMake
    add_test(test_all ${CMAKE_CURRENT_BINARY_DIR}/test_all)
endif()
```
