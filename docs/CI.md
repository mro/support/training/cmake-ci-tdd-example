# Continuous Integration

## Introduction

In this document we'll introduce step-by-step how to use [GitLab-CI](https://docs.gitlab.com/ee/ci/)
to implement **continuous integration** on top of an MCU based project.

The purpose of CI is to (not exhaustive):
- 👉 **enforce code-quality**
    - by running **static analysis** tools.
    - by running **code-styling** tools.
    - by generating **documentation**.
- 👉 ensure no **instabilities** or **regressions** are injected and project is still **live and functional**
    - by running **unit-tests** annd ensuring **coverage** is sufficient.
    - by generating the **build environment** and injecting changes on purpose
- ✨ ultimately prepare for **[Continuous Delivery](https://en.wikipedia.org/wiki/Continuous_delivery)** and/or **[Continuous Deployment](https://en.wikipedia.org/wiki/Continuous_deployment)**.


✋ Since this project has **no deployment**, no functional tests bench or production
environment, **Continuous Delivery** and **Continuous Deployment** are not
covered in this training.

## GitLab-CI basics

✋ This document is not intended to be a training on GitLab-CI itself, we'll focus
on main topics and implementing a pipeline that's tailored for this project.

To get more details about what GitLab-CI can offer, a [complete guide](https://docs.gitlab.com/ee/ci/)
and documentation are available.

👉 Yet, we'll introduce some basics to understand what's in our [.gitlab-ci.yml](../.gitlab-ci.yml) file !

✋ First of all, **don't be scared**, the file on this project is extra-long on purpose,
to be totally transparent and not hide any subtle hack under the hood, a **real live** is more like **[libccut/.gitlab-ci.yml](https://gitlab.cern.ch/mro/common/libraries/libccut/-/blob/master/.gitlab-ci.yml)**, but we'll see on how to achieve this below 😜.

A GitLab-CI pipeline is composed of:
- `stages` that gives main categories.
- `jobs` that are actual tasks/scripts to run.

**Stages** are declared at the beginning of your file (and can be whatever you want, it's really only about naming things):
```yml
stages:
  - build
  - test
  - package
  - deploy
```

**Jobs** are a bit more interesting, let's have a closer look 👀:
```yml
# Job name, will appear like that
build:
  # stage the job belongs to
  stage: build
  # container this job must run on (for container based builds)
  image: debian:stable-slim
  # some environment variables injected in the script below
  variables:
    CMAKE_OPTIONS: '-DCOVERAGE=ON'
  # running conditions for this job
  rules:
    # always run, and must succeed
    - allow_failure: false
  # script part, any failure (return code != 0) will fail the job
  script:
    - apt-get update
    - apt-get install -y build-essential git cmake libcppunit-dev ${DEPENDENCIES}

    - mkdir build && cd build
    - cmake .. ${CMAKE_OPTIONS} && make -j4
```

Creating a `.gitlab-ci.yml` file in your repository with this content, will:
- spawn a `debian:stable-slim` container on an available kubernetes cluster (some are publicly available on gitlab.com, hooking a private one is easy-peasy).
- install dependencies in that container
- invoke CMake and compile the project

And this will happen on every CI event (commit, merge-request, tag, etc...).

👉 With those few lines we already **raised our project quality** quite a lot ! ensuring that this project compiles on a basic *Debian* installing a bunch of dependencies.

A green pipeline ✅ guarantees that project can **easily be rebuilt** by repeating those steps,
we already stepped out of the developer's workspace providing some strong guarantees ! 🎉

## Build and unit-tests : because you like when **it works** !

Now have a look into our [.gitlab-ci.yml](../.gitlab-ci.yml) file.

We've already seen something similar to `fast-build` job in the lines above.

It is extended (read inherited) by `build` that changes the context:
```yml
build:
  # It will inherit all attributes from fast-build
  extends: fast-build
  # But will run in a dedicated image (that we'll see below)
  image: $CI_REGISTRY_IMAGE:latest
  # And it needs this image to be built before (if it has to be built in this pipeline)
  needs:
    - job: docker-latest
      optional: true
```

Now we have two build contexts, both are compiling on host, let's add another
one to build for our target:
```yml
avr-build:
  extends: build
  # That's where those variables make sense
  variables:
    CMAKE_OPTIONS: '-DUSE_AVR_TOOLCHAIN=ON'
  # Jobs are volatile, that's what we want to keep "artifacts"
  artifacts:
    paths:
      # Let's save binaries, will be accessible from GitLab's interface
      - build/**/*.eeprom
      - build/**/*.hex
      - build/src/plane
      - build/tests/integ_*
```

To run the tests, we could simply write a script that calls `make test`, but
there's a bit more processing to push results to GitLab:
- code-coverage results
- version badges
- test reports

👉 Check on `test` job in [.gitlab-ci.yml](../.gitlab-ci.yml) for this processing,
see how artifacts now also includes `reports`.

We've made most of the work, now let's keep-going !

## Style, Lint and Check : because you like it ✨ **shiny** ✨

Now all the efforts we made in CMake (see [CMake.md](./CMake.md)) to have generic rules start
to make sense, we simply have to connect everything altogether and enjoy 🍹

Some jobs are really straight-forward:
```yml
style:
  stage: test
  extends: build
  script:
    # Inject another script here, this is a GitLab-CI yml extension
    - !reference [build, script]
    # Run clang-format eventually modifying files
    - make style

    # That's only for informational purpose (to see what's wrong from logs)
    - git diff
    # If there are changes in the repo 👉 styling is KO
    - '[ $(git status -uno --porcelain | wc -l) -eq 0 ] || exit 1'
```

Others do require some data-processing to shape our tool's output in GitLab-CI
(see `lint` job in [.gitlab-ci.yml](../.gitlab-ci.yml).

👉 Have a look into a pipeline that was generated by this project : [#7377504](https://gitlab.cern.ch/mro/support/training/cmake-ci-tdd-example/-/pipelines/7377504/)

Test results, code-coverage, code-quality are all displayed, more important those
will be compared and displayed in merge-requests giving code-maintainers the
opportunity to catch regressions on the fly.

## Includes and CI libraries

As mentionned in the *Introduction*, projects don't really implement all this but
rather use `includes` :
```yml
# include those files in current yml
include:
  # project we refer to (on same GitLab server)
  - project: 'mro/common/tools/gitlab-ci-utils'
    file:
      # project's files to include
      - 'ci/scripts/gitlab-ci-cxx.yml'
      - 'ci/scripts/gitlab-ci-artifacts.yml'
```

✋ The project included above is [here](https://gitlab.cern.ch/mro/common/tools/gitlab-ci-utils),
don't hesitate to have a look into it and pick what you need.

Those *includes* implement only **template jobs** (name prefixed with a `.`)
that can be inherited by won't create jobs on their own.

👉 Using those includes and a common set of CMake rules (as described in [CMake](./CMake.md) rules),
your CI gets **heavily simplified**, having to configure only what differs from your "standard", ex: [libccut/.gitlab-ci.yml](https://gitlab.cern.ch/mro/common/libraries/libccut/-/blob/master/.gitlab-ci.yml)

## Building docker images

We've seen that most of the jobs above all relied on a `docker-latest` and were
refering to a `$CI_REGISTRY_IMAGE:latest` image.

This image is our build-image (as built in [Docker.md](./Docker.md)), it's being
prepared by the CI itself and made available both to other jobs and to end-users.

👉 Have a look into the dedicated GitLab menu: [cmake-ci-tdd-example/container_registry](https://gitlab.cern.ch/mro/support/training/cmake-ci-tdd-example/container_registry)

Building docker images from a docker container is not straight-forward, a complete article describes on how to achieve this with GitLab-CI
 [here](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#enable-docker-commands-in-your-cicd-jobs) (this is slightly out of scope for this training and depends on your environment).

## GitLab projects configuration

Going one step further, all our GitLab projects (tons of those) use the same rules
and are managed in a similar way.

We expect from those GitLab projects to be configured the same way : badges, merge-requests requirements, permissions, external notifications, etc..

But ensuring this is tedious, so we implemented a tool to do that for us : [gitlab-config](https://gitlab.cern.ch/mro/common/tools/gitlab-config). \
With our per-team configurations: [gitlab-config-mro](https://gitlab.cern.ch/mro/common/tools/gitlab-config-mro), [gitlab-config-ntof](https://gitlab.cern.ch/ntof/daq/common/tools/gitlab-config-ntof), [gitlab-config-apc](https://gitlab.cern.ch/apc/common/tools/gitlab-config-apc) (no worries, no secrets there, only keys displayed are public ones).

This plugin based tool will crawl GitLab groups and projects and configure projects
one by one.

You can re-use it if you're going for GitLab, but please if you update it do keep
us posted and send a merge-request, collaboration is always appreciated 🤩 !
