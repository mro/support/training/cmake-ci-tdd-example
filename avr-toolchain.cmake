

# program names
find_program(AVRCPP avr-g++ REQUIRED)
find_program(AVRC avr-gcc REQUIRED)
find_program(AVRSTRIP avr-strip REQUIRED)
find_program(OBJCOPY avr-objcopy REQUIRED)
find_program(OBJDUMP avr-objdump REQUIRED)
find_program(AVRSIZE avr-size REQUIRED)
find_program(AVRDUDE avrdude REQUIRED)

# Sets the compiler
set(CMAKE_SYSTEM_NAME  Generic)
set(CMAKE_C_COMPILER ${AVRC} CACHE FILEPATH "AVR C Compiler")
set(CMAKE_CXX_COMPILER ${AVRCPP} CACHE FILEPATH "AVR C++ Compiler")
set(CMAKE_ASM_COMPILER ${AVRC} CACHE FILEPATH "AVR ASM Compiler")

set(CMAKE_C_COMPILER_TARGET "avr")
set(CMAKE_CXX_COMPILER_TARGET "avr")
set(CMAKE_ASM_COMPILER_TARGET "avr")

set(CMAKE_EXECUTABLE_SUFFIX ".elf")

# Variables regarding the AVR chip
if(NOT DEFINED MCU)
    set(MCU "atmega328p")
endif()
set(MCU "${MCU}" CACHE STRING "target MCU, default: atmega328p")

if(NOT DEFINED F_CPU)
    set(F_CPU "16000000")
endif()
set(F_CPU "${F_CPU}" CACHE STRING "target CPU frequency, default: 16000000")

if(NOT DEFINED BAUD)
    set(BAUD 9600)
endif()
set(BAUD "${BAUD}" CACHE STRING "system console baudrate, default: 9600")

if(NOT DEFINED PROG_TYPE)
    set(PROG_TYPE "arduino")
endif()
set(PROG_TYPE "${PROG_TYPE}" CACHE STRING "avrdude programation type, default: arduino")

if(NOT DEFINED PROG_DEVICE)
    set(PROG_DEVICE "/dev/ttyACM0")
endif()
set(PROG_DEVICE "${PROG_DEVICE}" CACHE STRING "avrdude programation device, default: /dev/ttyACM0")

set(CMAKE_EXE_LINKER_FLAGS_INIT "-Wl,-u,vfprintf -lprintf_flt -lm")
set(CMAKE_C_FLAGS_INIT "${CMAKE_C_FLAGS_INIT} -Os -gstabs -mmcu=${MCU}  -DF_CPU=${F_CPU} -DBAUD=${BAUD}")
set(CMAKE_C_FLAGS_INIT "${CMAKE_C_FLAGS_INIT} -Wall -Wstrict-prototypes -Wl,--gc-sections -Wl,--relax")
set(CMAKE_C_FLAGS_INIT "${CMAKE_C_FLAGS_INIT} -funsigned-char -funsigned-bitfields -fpack-struct -fshort-enums -ffunction-sections -fdata-sections")
set(CMAKE_ASM_FLAGS_INIT "${CMAKE_C_FLAGS_INIT}")
set(CMAKE_CXX_FLAGS_INIT "${CMAKE_C_FLAGS_INIT}")
set(CMAKE_C_FLAGS_INIT "-std=gnu99 ${CMAKE_C_FLAGS_INIT}")

set(CMAKE_C_FLAGS_Debug_INIT "-g -ggdb")
set(CMAKE_CXX_FLAGS_Debug_INIT "-g -ggdb")

function(avr_prepare_executable TARGET_NAME)
    message(STATUS "Adding AVR build steps for ${TARGET_NAME} ...")
    if(NOT CMAKE_BUILD_TYPE STREQUAL "Debug")
        add_custom_command(TARGET "${TARGET_NAME}" POST_BUILD
            COMMAND ${AVRSTRIP} "$<TARGET_FILE:${TARGET_NAME}>"
            WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}")
    endif()

    add_custom_command(TARGET "${TARGET_NAME}" POST_BUILD
        COMMAND ${OBJCOPY} -R .eeprom -O ihex "$<TARGET_FILE:${TARGET_NAME}>" "${TARGET_NAME}.hex"
        WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
        BYPRODUCTS "${TARGET_NAME}.hex")

    add_custom_command(TARGET "${TARGET_NAME}" POST_BUILD
        COMMAND ${OBJCOPY} -j .eeprom --change-section-lma .eeprom=0 -O ihex "$<TARGET_FILE:${TARGET_NAME}>" "${TARGET_NAME}.eeprom"
        WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
        BYPRODUCTS "${TARGET_NAME}.eeprom")
endfunction()

function(add_flash_target TARGET_NAME EXECUTABLE_TARGET)
    add_custom_target(${TARGET_NAME} ${AVRDUDE} -c ${PROG_TYPE} -p ${MCU} -P ${PROG_DEVICE} -U flash:w:${EXECUTABLE_TARGET}.hex
        DEPENDS ${EXECUTABLE_TARGET})
endfunction()
