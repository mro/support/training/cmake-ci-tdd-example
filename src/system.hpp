/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-04-30T18:37:26
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef SYSTEM_HPP__
#define SYSTEM_HPP__

#include <stdint.h>
#include <time.h>

#if defined(__AVR_ATmega328P__)
#include <util/delay.h>
#else
#include <unistd.h>
#endif

#define NS_PER_SEC 1000000000ULL
#define US_PER_SEC 1000000ULL

#define CYCLES_PER_SEC ((uint64_t) (F_CPU))
#define CYCLES_PER_USEC (CYCLES_PER_SEC / 1000000)

#define CYCLES(time_ns) \
    (((CYCLES_PER_SEC * (time_ns)) + NS_PER_SEC - 1ULL) / NS_PER_SEC)

typedef uint64_t microsec_t;

#if defined(__BUILTIN_AVR_DELAY_CYCLES)
#define DELAY_CYCLES(count) __builtin_avr_delay_cycles(count);
#else
#define DELAY_CYCLES(count)                              \
    do                                                   \
    {                                                    \
        const struct timespec ts                         \
        {                                                \
            0, long(count * NS_PER_SEC / CYCLES_PER_SEC) \
        };                                               \
        nanosleep(&ts, 0);                               \
    } while (0)
#endif

void sys_init();

/**
 * @brief get current time in micro-seconds
 *
 * @return uint64_t
 */
microsec_t micros(void);

/**
 * @brief micro-seconds delay
 * @details not suitable for interrupt contexts (because of `micros()` use),
 * do use `_delay_us` in such cases.
 *
 * @param d delay to sleep for in micro-seconds
 */
static inline void delay_us(uint32_t d)
{
#if defined(__AVR_ATmega328P__)
    if (d > 1000)
    {
        microsec_t deadline;
        microsec_t now = micros();
        for (deadline = now + d - 250; deadline >= now; now = micros())
            _delay_us(50);
        d = deadline + 250 - now;
    }

#define DELAY_LOOP(X) \
    while (d >= (X))  \
    {                 \
        _delay_us(X); \
        d -= (X);     \
    }
    DELAY_LOOP(128);
    DELAY_LOOP(16);
#undef DELAY_LOOP
    if (d)
        while (--d > 0)
            _delay_us(1);
#else
    usleep(d);
#endif
}

#endif
