/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-04-29T17:07:40
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include "usart.hpp"

#include <avr/io.h>

#ifndef BAUD
#define BAUD 9600
#endif

static int usart_stdio_write(char c, FILE *_stream);

void usart_init()
{
    uint16_t ubrr = F_CPU / 16 / BAUD - 1;
    /*Set baud rate */
    UBRR0H = (unsigned char) (ubrr >> 8);
    UBRR0L = (unsigned char) ubrr;
    /* Enable receiver and transmitter */
    UCSR0B = (1 << RXEN0) | (1 << TXEN0);
    /* Set frame format: 8data, 2stop bit */
    UCSR0C = (1 << USBS0) | (3 << UCSZ00);

#if defined(__AVR_ATmega328P__)
    stdout = fdevopen(usart_stdio_write, NULL);
#endif
}

void usart_putchar(char c)
{
    if (c == '\n')
        usart_putchar('\r');
    loop_until_bit_is_set(UCSR0A, UDRE0);

    /* Put data into buffer, sends the data */
    UDR0 = c;
}

static int usart_stdio_write(char c, FILE * /*stream*/)
{
    usart_putchar(c);
    return 0;
}
