/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-05-02T14:20:10
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef MPU6050_HPP__
#define MPU6050_HPP__

#include <stdint.h>

#include "system.hpp"

#define CONFIG_REG 0x1A

#define GYRO_CONFIG_REG 0x1B
#define GC_SELF_TEST_BIT 5
#define GC_FS_SEL_BIT 3

#define ACCEL_CONFIG_REG 0x1C
#define AC_FS_SEL_BIT 3

#define PWR_MGMT_1_REG 0x6B

#define SAMPLE_RATE_DIV_REG 0x19

#define ACCEL_OUT_REG 0x3B
#define TEMP_OUT_REG 0x41
#define GYRO_OUT_REG 0x43

#define NB_AXIS 3
#define CALIB_OFFSET_NB_MEAS 500

class TWI;

class MPU6050
{
public:
    explicit MPU6050(TWI &twi, uint8_t address = defaultAddress);

    struct Data
    {
        float x;
        float y;
        float z;
    };

    bool init();

    bool update();
    bool fetch();

    inline const Data &gyro() const { return m_gyro; }
    inline const Data &accel() const { return m_accel; }
    inline float pitch() const { return m_pitch; }
    inline float roll() const { return m_roll; }

    static constexpr uint8_t defaultAddress = 0x68;

protected:
    void calibOffsets();

    TWI &m_twi;
    uint8_t m_address;
    microsec_t m_lastUpdate;

    Data m_accel;
    Data m_gyro;
    int16_t m_temperature;

    Data m_accel_offset;
    Data m_gyro_offset;

    float m_accel_ratio;
    float m_gyro_ratio;

    float m_pitch;
    float m_roll;
};

#endif