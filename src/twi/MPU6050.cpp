/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-05-02T14:26:52
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include "MPU6050.hpp"

#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>

#include "../TWI.hpp"

constexpr uint8_t MPU6050::defaultAddress;

MPU6050::MPU6050(TWI &twi, uint8_t address) :
    m_twi{twi},
    m_address{address},
    m_lastUpdate(0),
    m_accel{0, 0, 0},
    m_gyro{0, 0, 0},
    m_temperature(0),
    m_accel_offset{0, 0, 0},
    m_gyro_offset{0, 0, 0},
    m_accel_ratio{0},
    m_gyro_ratio{0},
    m_pitch{0},
    m_roll{0}
{}

static inline bool checkRc(uint8_t rc, const char *msg)
{
    if (rc != 0)
    {
        printf("MPU6050: failed to %s, code: %.2x\n", msg, rc);
        return false;
    }
    return true;
}

bool MPU6050::init()
{
    bool ret = true;
    uint8_t rc;

    rc = m_twi.writeReg(MPU6050::defaultAddress, PWR_MGMT_1_REG, 1);
    ret = checkRc(rc, "power-on") && ret;

    rc = m_twi.writeReg(MPU6050::defaultAddress, SAMPLE_RATE_DIV_REG, 0);
    ret = checkRc(rc, "set sample-div") && ret;

    rc = m_twi.writeReg(MPU6050::defaultAddress, CONFIG_REG, 0);
    ret = checkRc(rc, "configure device") && ret;

    uint8_t gc_fs_sel = 0;
    rc = m_twi.writeReg(MPU6050::defaultAddress, GYRO_CONFIG_REG,
                        (gc_fs_sel << GC_FS_SEL_BIT));
    ret = checkRc(rc, "configure gyro") && ret;
    m_gyro_ratio = ((INT16_MAX / (250.0 * (1 << gc_fs_sel))));

    uint8_t ac_fs_sel = 0;
    rc = m_twi.writeReg(MPU6050::defaultAddress, ACCEL_CONFIG_REG,
                        (ac_fs_sel << AC_FS_SEL_BIT));
    ret = checkRc(rc, "configure accel") && ret;
    m_accel_ratio = ((INT16_MAX >> (ac_fs_sel + 1)) + 1);

    calibOffsets();

    return ret;
}

void MPU6050::calibOffsets()
{
    printf("Calibrating MPU6050");
    fflush(stdout);
    m_accel_offset.x = m_accel_offset.y = m_accel_offset.z = 0;
    m_gyro_offset.x = m_gyro_offset.y = m_gyro_offset.z = 0;

    Data accel_sum{0, 0, 0};
    Data gyro_sum{0, 0, 0};

    for (size_t i = 0; i < CALIB_OFFSET_NB_MEAS; ++i)
    {
        if (!fetch())
        {
            puts(": failed");
            return;
        }
        accel_sum.x += m_accel.x;
        accel_sum.y += m_accel.y;
        accel_sum.z += m_accel.z - 1.0; /* gravity */

        gyro_sum.x += m_gyro.x;
        gyro_sum.y += m_gyro.y;
        gyro_sum.z += m_gyro.z;
        delay_us(1000);
    }

    m_accel_offset.x = accel_sum.x / CALIB_OFFSET_NB_MEAS;
    m_accel_offset.y = accel_sum.y / CALIB_OFFSET_NB_MEAS;
    m_accel_offset.z = accel_sum.z / CALIB_OFFSET_NB_MEAS;

    m_gyro_offset.x = gyro_sum.x / CALIB_OFFSET_NB_MEAS;
    m_gyro_offset.y = gyro_sum.y / CALIB_OFFSET_NB_MEAS;
    m_gyro_offset.z = gyro_sum.z / CALIB_OFFSET_NB_MEAS;

    m_pitch = 0;
    m_roll = 0;
    puts(": done");
}

bool MPU6050::fetch()
{
    uint8_t buffer[(NB_AXIS * 2 + 1) * sizeof(int16_t)];
    uint8_t rc;
    const uint8_t addr = ACCEL_OUT_REG;

    rc = m_twi.read(m_address, &addr, 1, buffer, sizeof(buffer));
    if (rc != 0)
        fprintf(stderr, "failed to fetch, code: %.2x\n", rc);
    else
    {
        m_accel.x = float(int16_t((buffer[0] << 8) | buffer[1])) /
                m_accel_ratio -
            m_accel_offset.x;
        m_accel.y = float(int16_t((buffer[2] << 8) | buffer[3])) /
                m_accel_ratio -
            m_accel_offset.y;
        m_accel.z = -float(int16_t((buffer[4] << 8) | buffer[5])) /
                m_accel_ratio -
            m_accel_offset.z;

        m_temperature = (buffer[6] << 8) | buffer[7];

        m_gyro.x = float((buffer[8] << 8) | buffer[9]) / m_gyro_ratio -
            m_gyro_offset.x;
        m_gyro.y = float((buffer[10] << 8) | buffer[11]) / m_gyro_ratio -
            m_gyro_offset.y;
        m_gyro.z = float((buffer[12] << 8) | buffer[13]) / m_gyro_ratio -
            m_gyro_offset.z;
    }

    return rc == 0;
}

bool MPU6050::update()
{
    if (!fetch())
        return false;

    m_roll = -atan2f(m_accel.x, hypotf(m_accel.z, m_accel.y)) * (180 / M_PI);
    m_pitch = atan2f(m_accel.y, hypotf(m_accel.z, m_accel.x)) * (180 / M_PI);
    return true;
}
