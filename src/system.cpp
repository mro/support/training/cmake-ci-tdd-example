/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-04-30T17:10:09
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include "system.hpp"

#include <avr/interrupt.h>
#include <avr/io.h>

#include "usart.hpp"

#if defined(__AVR_ATmega328P__)

#define TIMER0_PRESCALER 64
#define TIMER0_TICK_US (TIMER0_PRESCALER / CYCLES_PER_USEC)
#define TIMER0_OVERFLOW_US (TIMER0_PRESCALER * 256 / CYCLES_PER_USEC)

volatile unsigned long timer0_overflow_count = 0;

void sys_init()
{
    /* hook stdio */
    usart_init();
    sei();

    /* fast pwm mode */
    TCCR0A |= (1 << WGM01) | (1 << WGM00);

    /* clk/64 input */
    TCCR0B |= (0 << CS02) | (1 << CS01) | (1 << CS00);

    /* interrupt on overflow */
    TIMSK0 |= (1 << TOIE0);
}

ISR(TIMER0_OVF_vect)
{
    ++timer0_overflow_count;
}

microsec_t micros()
{
    uint64_t m;
    uint8_t t;
    uint8_t oldSREG = SREG;

    cli();
    m = timer0_overflow_count;
    t = TCNT0;

    SREG = oldSREG;

    return ((m << 8) | t) * TIMER0_TICK_US;
}

#else

void sys_init() {}

microsec_t micros()
{
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);

    return ts.tv_sec * 1000000 + ts.tv_nsec / 1000;
}

#endif