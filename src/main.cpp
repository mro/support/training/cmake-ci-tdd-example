/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-04-29T17:33:56
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include <avr/io.h>
#include <stdbool.h>

#include "Plane.hpp"

int main(void)
{
    sys_init();
    Plane plane;

    while (1)
    {
        plane.loop();
        delay_us(1000);
    }
}
