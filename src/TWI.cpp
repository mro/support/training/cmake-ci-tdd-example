/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-04-29T17:55:06
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include "TWI.hpp"

#include <avr/io.h>
#include <stdio.h>
#include <util/twi.h>

TWI::TWI()
{
    /* prescaler = 1 */
    TWSR = 0;
    TWBR = ((F_CPU / TWI::Frequency) - 16) / 2;

    TWCR = (1 << TWEN) | (1 << TWEA);
}

uint8_t TWI::read(uint8_t address,
                  const uint8_t *wdata,
                  uint16_t wdata_count,
                  uint8_t *rdata,
                  uint16_t rdata_count)
{
    uint8_t status = 0;
    uint16_t counter;

    if (wdata_count > 0)
    {
        TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTA); // Send START
        while (!(TWCR & (1 << TWINT)))
            ;

        status = TW_STATUS;
        if (status != TW_START && status != TW_REP_START)
            return status;

        TWDR = (address << 1) | TW_WRITE;
        TWCR = (1 << TWINT) | (1 << TWEN);
        while (!(TWCR & (1 << TWINT)))
            ;

        status = TW_STATUS;
        if (status == TW_MT_SLA_NACK)
            goto twi_stop;
        else if (status != TW_MT_SLA_ACK)
            goto twi_err;

        for (counter = 0; counter < wdata_count; ++counter)
        {
            TWDR = wdata[counter];
            TWCR = (1 << TWINT) | (1 << TWEN);
            while (!(TWCR & (1 << TWINT)))
                ;

            status = TW_STATUS;
            if (status == TW_MT_DATA_NACK)
                goto twi_stop;
            else if (status != TW_MT_DATA_ACK)
                goto twi_err;
        }
    }

    if (rdata_count > 0)
    {
        TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTA); // Send START
        while (!(TWCR & (1 << TWINT)))
            ;
        status = TW_STATUS;
        if (wdata_count && status != TW_REP_START)
            return status;
        else if (!wdata_count && status != TW_REP_START)
            return status;

        TWDR = (address << 1) | TW_READ;
        TWCR = (1 << TWINT) | (1 << TWEN);
        while (!(TWCR & (1 << TWINT)))
            ;
        status = TW_STATUS;
        if (status == TW_MR_SLA_NACK)
            goto twi_stop;
        else if (status != TW_MR_SLA_ACK)
            goto twi_err;

        for (counter = 0; counter < rdata_count - 1; ++counter)
        {
            TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWEA);
            while (!(TWCR & (1 << TWINT)))
                ;
            status = TW_STATUS;
            if (status != TW_MR_DATA_ACK)
                goto twi_err;
            rdata[counter] = TWDR;
        }

        TWCR = (1 << TWINT) | (1 << TWEN);
        while (!(TWCR & (1 << TWINT)))
            ;
        status = TW_STATUS;
        if (status != TW_MR_DATA_NACK)
            goto twi_err;
        rdata[counter] = TWDR;
    }

    status = 0;

twi_stop:
    if (rdata_count || wdata_count)
        TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTO); // Send STOP

twi_err:
    return status;
}
