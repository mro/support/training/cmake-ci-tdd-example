/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-04-30T11:20:29
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef FABLED_HPP__
#define FABLED_HPP__

#include <avr/interrupt.h>
#include <stddef.h>
#include <stdint.h>
#include <time.h>

#include "port.hpp"
#include "system.hpp"

typedef struct rgb_t
{
    uint8_t red;
    uint8_t green;
    uint8_t blue;
} __attribute__((packed)) rgb;

typedef struct grb_t
{
    uint8_t green;
    uint8_t red;
    uint8_t blue;
} __attribute__((packed)) grb;

template<typename TYPE, size_t LED_COUNT, PORT P, size_t PIN>
class FABLed
{
public:
    static constexpr size_t led_count = LED_COUNT;

    FABLed() : m_nextUpdate{0}
    {
        getDDR<P>() |= (1 << PIN);
        for (size_t i = 0; i < LED_COUNT * 3; ++i)
            m_leds[i] = 0;
    }

    template<typename RGB>
    RGB &led(size_t i)
    {
        return *reinterpret_cast<RGB *>(m_leds + i * 3);
    }

    void refresh();

    void clear();

    void inc(size_t led);

    void testPattern();

    // protected:
    microsec_t m_nextUpdate;
    uint8_t m_leds[3 * LED_COUNT];
};

struct WS2812B
{
    struct one
    {
        static constexpr uint32_t high = CYCLES(900);
        static constexpr uint32_t low = CYCLES(200);
    };
    struct zero
    {
        static constexpr uint32_t high = CYCLES(150);
        static constexpr uint32_t low = CYCLES(950);
    };
    static constexpr microsec_t idleUs{75};
};

template<typename TYPE, size_t LED_COUNT, PORT P, size_t PIN>
void FABLed<TYPE, LED_COUNT, P, PIN>::refresh()
{
    microsec_t now = micros();
    if (now < m_nextUpdate)
        delay_us(m_nextUpdate - now);

    uint8_t oldSREG = SREG;
    cli();

    for (uint16_t c = 0; c < LED_COUNT * 3; c++)
    {
        const uint8_t val = m_leds[c];
        for (int8_t b = 7; b >= 0; b--)
        {
            const bool bit = (val >> b) & 0x1;

            if (bit)
            {
                // HIGH with ASM sbi (2 words, 2 cycles)
                getPORT<P>() |= (1 << PIN);
                // Wait exact number of cycles specified
                DELAY_CYCLES(TYPE::one::high - 2);
                //  LOW with ASM cbi (2 words, 2 cycles)
                getPORT<P>() &= ~(1 << PIN);
                // Wait exact number of cycles specified
                DELAY_CYCLES(TYPE::one::low - 2);
            }
            else
            {
                // Send a ZERO

                // HIGH with ASM sbi (2 words, 2 cycles)
                getPORT<P>() |= (1 << PIN);
                // Wait exact number of cycles specified
                DELAY_CYCLES(TYPE::zero::high - 2);
                //  LOW with ASM cbi (2 words, 2 cycles)
                getPORT<P>() &= ~(1 << PIN);
                // Wait exact number of cycles specified
                DELAY_CYCLES(TYPE::zero::low - 2);
            }
        }
    }
    SREG = oldSREG;

    m_nextUpdate = micros() + TYPE::idleUs;
}

template<typename TYPE, size_t LED_COUNT, PORT P, size_t PIN>
void FABLed<TYPE, LED_COUNT, P, PIN>::clear()
{
    for (size_t i = 0; i < 3 * led_count; ++i)
        m_leds[i] = 0;
}

template<typename TYPE, size_t LED_COUNT, PORT P, size_t PIN>
void FABLed<TYPE, LED_COUNT, P, PIN>::inc(size_t led_idx)
{
    rgb &led_device = led<rgb>(led_idx);

    if (led_device.blue)
        ++led_device.blue;
    else if (led_device.green)
    {
        if (!++led_device.green)
            ++led_device.blue;
    }
    else if (!++led_device.red)
        ++led_device.green;
}

template<typename TYPE, size_t LED_COUNT, PORT P, size_t PIN>
void FABLed<TYPE, LED_COUNT, P, PIN>::testPattern()
{
    for (size_t led_idx = 0; led_idx < LED_COUNT; ++led_idx)
    {
        inc(led_idx);
    }
    refresh();
}

#endif