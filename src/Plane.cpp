/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-05-10T08:56:08
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include "Plane.hpp"

#include <math.h>
#include <stdio.h>

#include "system.hpp"

template<int min, int max>
inline float normalize(float value)
{
    if (value < min)
        return 0.0;
    else if (value > max)
        return 1.0;
    else
        return (value - min) / (max - min);
}

template<typename T>
inline T max(T t1, T t2)
{
    return (t2 > t1) ? t2 : t1;
}

Plane::Plane() : m_twi{}, m_gyro(m_twi), m_lastUpdate{0}, m_blinkStart{0}
{
    puts("Initializing plane");
    init();
}

void Plane::init()
{
    m_big_leds.clear();
    m_small_leds.clear();
    m_gyro.init();
}

void Plane::loop()
{
    microsec_t now = micros();

    m_gyro.update();

    const float pitch{m_gyro.pitch()}, roll{m_gyro.roll()};

    m_small_leds.led<grb>(pitchLed).red = uint8_t(
        max(normalize<20, 90>(pitch), (1 - normalize<-90, -20>(pitch))) * 0xFF);
    m_small_leds.led<grb>(rollRightLed).red = uint8_t(
        (1 - normalize<-90, -20>(roll)) * 0xFF);
    m_small_leds.led<grb>(rollLeftLed).red = uint8_t(normalize<20, 90>(roll) *
                                                     0xFF);

    /* blink once every 4 secs */
    if (((now / 1000000) % 4) == 0)
    {
        if (!m_blinkStart)
            m_blinkStart = now;
        int8_t value = 0xFF -
            abs(int(((now - m_blinkStart) * 0x1FE / 1000000) % 0x1FF) - 0xFF);
        for (size_t i = 0; i < m_big_leds.led_count; ++i)
            m_big_leds.led<rgb>(i).blue = value;
    }
    else if (m_blinkStart)
    {
        m_blinkStart = 0;
        for (size_t i = 0; i < m_big_leds.led_count; ++i)
            m_big_leds.led<rgb>(i).blue = 0;
    }

    m_small_leds.refresh();
    m_big_leds.refresh();
    m_lastUpdate = micros();
}