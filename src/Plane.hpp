/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-05-10T08:41:15
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef PLANE_HPP__
#define PLANE_HPP__

#include "FABLed.hpp"
#include "TWI.hpp"
#include "port.hpp"
#include "system.hpp"
#include "twi/MPU6050.hpp"

class Plane
{
public:
    Plane();

    void init();
    void loop();

    static constexpr size_t pitchLed{0};
    static constexpr size_t rollLeftLed{2};
    static constexpr size_t rollRightLed{1};

protected:
    TWI m_twi;
    MPU6050 m_gyro;
    FABLed<WS2812B, 3, PORT::B, PB4> m_big_leds;
    FABLed<WS2812B, 3, PORT::B, PB3> m_small_leds;
    microsec_t m_lastUpdate;
    microsec_t m_blinkStart;
};

#endif