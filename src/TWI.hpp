/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-04-29T17:54:43
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef TWI_HPP__
#define TWI_HPP__

#include <stdint.h>

/**
 * @brief Two Wire Interface, also known as I2C
 */
class TWI
{
public:
    TWI();

    /**
     * @brief general purpose read/write method
     *
     * @param[in] address device to send command to
     * @param[in] wdata write buffer
     * @param[in] wdata_count bytes of data to write
     * @param[out] rdata read buffer pointer
     * @param[in] rdata_count bytes of data to read
     * @return status error code (see
     * https://www.nongnu.org/avr-libc/user-manual/group__util__twi.html)
     */
    uint8_t read(uint8_t device,
                 const uint8_t *wdata,
                 uint16_t wdata_count,
                 uint8_t *rdata,
                 uint16_t rdata_count);

    inline uint8_t writeReg(uint8_t device, uint8_t addr, uint8_t value)
    {
        uint8_t wdata[2] = {addr, value};
        return read(device, wdata, 2, 0, 0);
    }

    inline uint8_t readReg(uint8_t device, uint8_t addr, uint8_t &value)
    {
        return read(device, &addr, 1, &value, 1);
    }

    static constexpr unsigned long Frequency = 100000L;
};

#endif