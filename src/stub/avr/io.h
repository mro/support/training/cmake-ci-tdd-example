/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-04-29T16:53:15
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef IO_H__
#define IO_H__

#include <stdint.h>

#define F_CPU 16000000

/* USART */
extern volatile uint8_t UBRR0H;
extern volatile uint8_t UBRR0L;

extern volatile uint8_t UCSR0B;
#define RXEN0 4
#define TXEN0 3

extern volatile uint8_t UCSR0C;
#define USBS0 3
#define UCSZ00 1

extern volatile uint8_t UCSR0A;
#define UDRE0 5

extern volatile uint8_t UDR0;

#define loop_until_bit_is_set(sfr, bit) \
    do                                  \
    {                                   \
    } while (!(sfr & (1 << bit)))

/* ports */
extern volatile uint8_t PORTB;
extern volatile uint8_t DDRB;
#define PB3 3
#define PB4 4
#define PB5 5

extern volatile uint8_t PORTC;
extern volatile uint8_t DDRC;

extern volatile uint8_t PORTD;
extern volatile uint8_t DDRD;
extern volatile uint8_t PIND;
#define PD2 2

/* interrupts */
extern volatile uint8_t SREG;

#endif