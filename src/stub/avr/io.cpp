/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-05-05T22:39:50
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include "io.h"

volatile uint8_t UBRR0H = 0;
volatile uint8_t UBRR0L = 0;

volatile uint8_t UCSR0B = 0;
volatile uint8_t UCSR0C = 0;
volatile uint8_t UCSR0A = 0;
volatile uint8_t UDR0 = 0;
volatile uint8_t PORTB = 0;
volatile uint8_t DDRB = 0;
volatile uint8_t PORTC = 0;
volatile uint8_t DDRC = 0;
volatile uint8_t PORTD = 0;
volatile uint8_t DDRD = 0;
volatile uint8_t PIND = 0;

volatile uint8_t SREG = 0;
