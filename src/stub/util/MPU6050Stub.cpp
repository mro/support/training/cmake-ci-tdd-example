/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-05-10T10:47:23
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include "MPU6050Stub.hpp"

#define ACCEL_OUT_REG_X 0x3B
#define ACCEL_OUT_REG_Y 0x3D
#define ACCEL_OUT_REG_Z 0x3F
#define ACCEL_CONFIG_REG 0x1C
#define AC_FS_SEL_BIT 3

constexpr uint8_t MPU6050Stub::defaultAddress;
constexpr size_t MPU6050Stub::memSize;

MPU6050Stub::MPU6050Stub() : TWIStubDevice(defaultAddress, memSize) {}

void MPU6050Stub::setAccel(Axis axis, float value)
{
    value = value * getAccelCoef();
    regs[ACCEL_OUT_REG_X + axis * 2] = (uint16_t(value) >> 8) & 0xFF;
    regs[ACCEL_OUT_REG_X + axis * 2 + 1] = uint16_t(value) & 0xFF;
}

uint16_t MPU6050Stub::getAccelCoef() const
{
    uint16_t ac_fs_sel = (regs[ACCEL_CONFIG_REG] >> AC_FS_SEL_BIT) & 0x03;
    return ((INT16_MAX >> (ac_fs_sel + 1)) + 1);
}
