/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-05-10T10:45:08
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef MPU6050STUB_HPP__
#define MPU6050STUB_HPP__

#include "twi-stub.hpp"

class MPU6050Stub : public TWIStubDevice
{
public:
    MPU6050Stub();

    enum Axis
    {
        X = 0,
        Y = 1,
        Z = 2
    };

    void setAccel(Axis axis, float value);

    uint16_t getAccelCoef() const;

    static constexpr uint8_t defaultAddress{0x68};
    static constexpr size_t memSize{0x76};
};

#endif