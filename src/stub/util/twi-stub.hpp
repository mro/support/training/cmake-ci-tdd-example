/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-05-07T17:13:45
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef TWI_STUB_HPP__
#define TWI_STUB_HPP__

#include <map>
#include <memory>
#include <vector>

class TWIStubDevice;

class TWIStub
{
public:
    typedef uint8_t Addr;

    static TWIStub &instance();

    void add(const std::shared_ptr<TWIStubDevice> &device);

    void remove(Addr addr);

    uint8_t setTWCR(uint8_t value);

    enum class Operation
    {
        NONE,
        WRITE,
        READ
    };

protected:
    TWIStub();

    std::map<Addr, std::shared_ptr<TWIStubDevice>> m_devices;
    Operation m_operation;
    std::shared_ptr<TWIStubDevice> m_dev;
    bool m_started;
};

class TWIStubDevice
{
public:
    typedef TWIStub::Addr Addr;

    TWIStubDevice(TWIStub::Addr addr, size_t size);

    virtual void start();

    virtual void stop();

    virtual bool write(uint8_t value);

    virtual bool read(volatile uint8_t &value);

    const TWIStub::Addr addr;

    std::vector<uint8_t> regs;
    uint8_t ptr;
    bool isPtrSet;
};

#endif