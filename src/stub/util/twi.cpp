/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-05-05T22:45:25
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include "twi.h"

#include <cstdint>

#include <stdio.h>

#include "twi-stub.hpp"

#define BIT(X) (1 << X)

volatile uint8_t TWSR;
volatile uint8_t TWBR;
volatile uint8_t TWDR;

twcr_t TWCR;

twcr_t &twcr_t::operator=(uint8_t value)
{
    m_value = TWIStub::instance().setTWCR(value);
    return *this;
}

TWIStub::TWIStub() : m_operation{Operation::NONE}, m_started{false} {}

TWIStub &TWIStub::instance()
{
    static TWIStub t;
    return t;
}

void TWIStub::add(const std::shared_ptr<TWIStubDevice> &device)
{
    m_devices[device->addr] = device;
}

void TWIStub::remove(Addr addr)
{
    m_devices.erase(addr);
}

uint8_t TWIStub::setTWCR(uint8_t value)
{
    if (!(value & BIT(TWINT)))
    {
        if (value != (BIT(TWEA) | BIT(TWEN)))
            fprintf(stderr, "TWIStub: TWINT not set %.2X\n", value);
        return value;
    }
    else if (!(value & BIT(TWEN)))
    {
        fprintf(stderr, "TWIStub: TWI disabled %.2X\n", value);
        return value;
    }

    if (value & BIT(TWSTA))
    {
        TWSR = (m_dev) ? TW_REP_START : TW_START;
        m_operation = Operation::NONE;
        m_started = true;
    }
    else if (value & BIT(TWSTO))
    {
        if (m_dev)
            m_dev->stop();
        m_dev.reset();
        m_operation = Operation::NONE;
        m_started = false;
    }
    else if (!m_started)
        return value;
    else if (!m_dev || m_operation == Operation::NONE)
    {
        auto it = m_devices.find(TWDR >> 1);
        if (it != m_devices.end())
        {
            m_dev = it->second;

            if (TWDR & TW_READ)
            {
                m_operation = Operation::READ;
                TWSR = TW_MR_SLA_ACK;
            }
            else
            {
                m_operation = Operation::WRITE;
                TWSR = TW_MT_SLA_ACK;
            }
        }
        else
            TWSR = (TWDR & TW_READ) ? TW_MR_SLA_NACK : TW_MT_SLA_NACK;
    }
    else if (m_operation == Operation::WRITE)
        TWSR = m_dev->write(TWDR) ? TW_MT_DATA_ACK : TW_MT_DATA_NACK;
    else if (m_operation == Operation::READ)
    {
        TWSR = (m_dev->read(TWDR) && (value & BIT(TWEA))) ? TW_MR_DATA_ACK :
                                                            TW_MR_DATA_NACK;
    }

    value &= ~(BIT(TWSTA) | BIT(TWSTO) | BIT(TWEA));
    value |= BIT(TWINT);
    return value;
}

TWIStubDevice::TWIStubDevice(Addr addr, size_t size) :
    addr{addr}, regs(size, 0), isPtrSet{false}
{}

void TWIStubDevice::start() {}

void TWIStubDevice::stop()
{
    isPtrSet = false;
}

bool TWIStubDevice::write(uint8_t value)
{
    if (!isPtrSet)
    {
        ptr = value;
        isPtrSet = true;
    }
    else if (ptr < regs.size())
        regs[ptr++] = value;
    else
        return false;
    return true;
}

bool TWIStubDevice::read(volatile uint8_t &value)
{
    if (!isPtrSet)
        return false;
    else if (ptr < regs.size())
        value = regs[ptr++];
    else
        return false;
    return true;
}
