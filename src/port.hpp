/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-04-30T19:37:37
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef PORT_HPP__
#define PORT_HPP__

#include <avr/io.h>
#include <stdint.h>

enum class PORT
{
    B,
    C,
    D
};

template<PORT P>
inline volatile uint8_t &getDDR();

template<PORT P>
inline volatile uint8_t &getPORT();

#define DEFINE_PORT(P, LP)                \
    template<>                            \
    inline volatile uint8_t &getDDR<P>()  \
    {                                     \
        return DDR##LP;                   \
    }                                     \
    template<>                            \
    inline volatile uint8_t &getPORT<P>() \
    {                                     \
        return PORT##LP;                  \
    }

DEFINE_PORT(PORT::B, B)
DEFINE_PORT(PORT::C, C)
DEFINE_PORT(PORT::D, D)

#endif