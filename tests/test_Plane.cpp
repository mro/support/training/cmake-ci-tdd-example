/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-05-10T10:44:13
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include <cppunit/TestAssert.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Plane.hpp"
#include "system.hpp"
#include "test_helpers.hpp"
#include "twi/MPU6050.hpp"
#include "util/MPU6050Stub.hpp"
#include "util/twi-stub.hpp"

/**
 * @brief Device Under Test Plane object
 * @details use inheritance to expose internals that are not exposed in regular
 * application.
 */
class DUTPlane : public Plane
{
public:
    using Plane::m_big_leds;
    using Plane::m_small_leds;
};

class TestPlane : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestPlane);
    CPPUNIT_TEST(blinkingLeds);
    CPPUNIT_TEST(attitude);
    CPPUNIT_TEST_SUITE_END();

    std::shared_ptr<MPU6050Stub> m_gyroStub;

public:
    void setUp()
    {
        sys_init();
        m_gyroStub = std::make_shared<MPU6050Stub>();
        TWIStub::instance().add(m_gyroStub);

        /* sensor is upside-down */
        m_gyroStub->setAccel(MPU6050Stub::Z, -1);
    }

    void tearDown()
    {
        TWIStub::instance().remove(MPU6050::defaultAddress);
        m_gyroStub.reset();
    }

    void blinkingLeds()
    {
        DUTPlane plane;

        microsec_t now;
        microsec_t deadline = micros() + US_PER_SEC * 5;
        for (now = micros(); now < deadline; now = micros())
        {
            plane.loop();
            if (plane.m_big_leds.led<rgb>(0).blue >= 0xF0)
                break;
            delay_us(100);
        }

        ASSERT_MSG("big led did not turn blue",
                   plane.m_big_leds.led<rgb>(0).blue >= 0xF0);
        microsec_t t1{now};

        delay_us(US_PER_SEC * 2);
        plane.loop();
        EQ_MSG("big led did not turn off", uint8_t(0x0),
               plane.m_big_leds.led<rgb>(0).blue);

        deadline = micros() + US_PER_SEC * 5;
        for (now = micros(); now < deadline; now = micros())
        {
            plane.loop();
            if (plane.m_big_leds.led<rgb>(0).blue >= 0xF0)
                break;
            delay_us(100);
        }
        ASSERT_MSG("big led did not turn blue again",
                   plane.m_big_leds.led<rgb>(0).blue >= 0xF0);
        int64_t interval = now - t1;
        const microsec_t freq = 4 * US_PER_SEC;

        ASSERT_MSG("invalid blinking frequency: " + std::to_string(interval),
                   interval >= (freq - US_PER_SEC / 2) &&
                       interval <= (freq + US_PER_SEC / 2));
    }

    void attitude()
    {
        DUTPlane plane;
        decltype(plane.m_small_leds) &leds{plane.m_small_leds};

        plane.loop();
        EQ(uint8_t(0), leds.led<grb>(Plane::pitchLed).red);
        EQ(uint8_t(0), leds.led<grb>(Plane::rollLeftLed).red);
        EQ(uint8_t(0), leds.led<grb>(Plane::rollRightLed).red);

        m_gyroStub->setAccel(MPU6050Stub::Y, 1);
        m_gyroStub->setAccel(MPU6050Stub::Z, 0);
        plane.loop();
        EQ(uint8_t(0xFF), leds.led<grb>(Plane::pitchLed).red);
        EQ(uint8_t(0), leds.led<grb>(Plane::rollLeftLed).red);
        EQ(uint8_t(0), leds.led<grb>(Plane::rollRightLed).red);

        m_gyroStub->setAccel(MPU6050Stub::X, 1);
        m_gyroStub->setAccel(MPU6050Stub::Y, 0);
        m_gyroStub->setAccel(MPU6050Stub::Z, 0);
        plane.loop();
        EQ(uint8_t(0), leds.led<grb>(Plane::pitchLed).red);
        EQ(uint8_t(0), leds.led<grb>(Plane::rollLeftLed).red);
        EQ(uint8_t(0xFF), leds.led<grb>(Plane::rollRightLed).red);

        m_gyroStub->setAccel(MPU6050Stub::X, -0.5);
        m_gyroStub->setAccel(MPU6050Stub::Y, -0.5);
        m_gyroStub->setAccel(MPU6050Stub::Z, 0);
        plane.loop();
        GREATEREQ(uint8_t(50), leds.led<grb>(Plane::pitchLed).red);
        GREATEREQ(uint8_t(50), leds.led<grb>(Plane::rollLeftLed).red);
        EQ(uint8_t(0), leds.led<grb>(Plane::rollRightLed).red);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestPlane);