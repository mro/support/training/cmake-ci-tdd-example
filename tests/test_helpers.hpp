/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-05-11T08:29:52
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef TEST_HELPERS_HPP__
#define TEST_HELPERS_HPP__

#include <cppunit/TestAssert.h>
#include <cppunit/extensions/HelperMacros.h>

#define EQ CPPUNIT_ASSERT_EQUAL
#define EQ_MSG CPPUNIT_ASSERT_EQUAL_MESSAGE
#define ASSERT CPPUNIT_ASSERT
#define ASSERT_MSG CPPUNIT_ASSERT_MESSAGE
#define GREATEREQ CPPUNIT_ASSERT_GREATEREQUAL
#define LESSEQ CPPUNIT_ASSERT_LESSEQUAL
#define ASSERT_THROW CPPUNIT_ASSERT_THROW
#define ASSERT_THROW_MSG CPPUNIT_ASSERT_THROW_MESSAGE
#define ASSERT_NO_THROW_MSG CPPUNIT_ASSERT_NO_THROW_MESSAGE
#define DBL_EQ CPPUNIT_ASSERT_DOUBLES_EQUAL
#define DBL_EQ_MSG CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE
#define FAIL CPPUNIT_FAIL

#endif