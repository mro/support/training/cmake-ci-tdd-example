/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-05-07T22:54:15
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include <cppunit/TestAssert.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "TWI.hpp"
#include "system.hpp"
#include "test_helpers.hpp"
#include "twi/MPU6050.hpp"
#include "util/MPU6050Stub.hpp"
#include "util/twi-stub.hpp"

class TestMPU6050 : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestMPU6050);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST_SUITE_END();

    std::shared_ptr<MPU6050Stub> m_stub;

public:
    void setUp()
    {
        sys_init();
        m_stub = std::make_shared<MPU6050Stub>();
        TWIStub::instance().add(m_stub);

        /* sensor is upside-down */
        m_stub->setAccel(MPU6050Stub::Z, -1);
    }

    void tearDown()
    {
        TWIStub::instance().remove(MPU6050::defaultAddress);
        m_stub.reset();
    }

    void simple()
    {
        TWI twi;
        MPU6050 gyro(twi);

        ASSERT(gyro.init());

        ASSERT(gyro.update());
        DBL_EQ_MSG("invalid initial pitch", 0, gyro.pitch(), 0.01);
        DBL_EQ_MSG("invalid initial roll", 0, gyro.roll(), 0.01);

        m_stub->setAccel(MPU6050Stub::Y, 1);
        m_stub->setAccel(MPU6050Stub::Z, 0);
        ASSERT(gyro.update());
        DBL_EQ_MSG("invalid pitch for Y=1", 90.0, gyro.pitch(), 0.01);
        DBL_EQ_MSG("invalid roll for Y=1", 0.0, gyro.roll(), 0.01);

        m_stub->setAccel(MPU6050Stub::X, -0.5);
        m_stub->setAccel(MPU6050Stub::Y, 0.5);
        m_stub->setAccel(MPU6050Stub::Z, 0);
        ASSERT(gyro.update());
        DBL_EQ_MSG("invalid pitch for 45 deg", 45.0, gyro.pitch(), 0.01);
        DBL_EQ_MSG("invalid roll for 45 deg", 45.0, gyro.roll(), 0.01);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestMPU6050);