/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-05-02T14:53:36
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>

#include "FABLed.hpp"
#include "system.hpp"

bool test_delay(microsec_t duration, microsec_t delta)
{
    printf("TEST: delay and clocking, %lu usecs...", (long unsigned) (duration));
    fflush(stdout);
    microsec_t t1 = micros();
    delay_us(duration);
    microsec_t t2 = micros();

    long diff = (t2 - t1) - duration;
    if (labs(diff) >= delta)
    {
        puts(": KO");
        printf("  failure in delay/clock implementation: %li us difference\n",
               diff);
        return false;
    }
    puts(": OK");
    printf("  clock delta: %li us difference\n", diff);
    return true;
}

int main()
{
    sys_init();
    DDRD &= ~(1 << PD2); // input
    PORTD |= (1 << PD2); // pull-up

    while (1)
    {
        puts("");
        puts("Press button to start test");
        fflush(stdout);

        while ((PIND & (1 << PD2)))
            delay_us(1);

        bool ok = true;
        ok &= test_delay(250, 25);
        ok &= test_delay(500, 25);

        ok &= test_delay(10000, 250);
        ok &= test_delay(1 * US_PER_SEC, 500);
        ok &= test_delay(5 * US_PER_SEC, 500);
        ok &= test_delay(10 * US_PER_SEC, 500);
        ok &= test_delay(60 * US_PER_SEC, 500);

        puts("");
        puts(ok ? "RESULT: OK" : "RESULT: KO");

        fflush(stdout);
    }
}
