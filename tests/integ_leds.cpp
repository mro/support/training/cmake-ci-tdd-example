/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-05-02T09:23:05
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include <stdio.h>

#include "FABLed.hpp"
#include "system.hpp"

template<typename LED>
void test(LED &led, const char *name, uint32_t delay)
{
    for (size_t idx = 0; idx <= led.led_count; ++idx)
    {
        if (idx == led.led_count)
            printf("Blinking all %s leds...\n", name);
        else
            printf("Blinking %s led %i\n", name, int(idx));
        fflush(stdout);

        for (size_t increments = 0; increments <= 0xFF * 3; ++increments)
        {
            if (idx < led.led_count)
                led.inc(idx);
            else
                led.testPattern();
            led.refresh();
            if (delay)
                delay_us(delay);
        }
    }
}

int main()
{
    sys_init();
    FABLed<WS2812B, 3, PORT::B, PB4> big_leds;
    FABLed<WS2812B, 3, PORT::B, PB3> small_leds;

    while (1)
    {
        big_leds.clear();
        small_leds.clear();

        test(big_leds, "big", 2000);
        test(small_leds, "small", 2000);

        test(big_leds, "big (fast)", 0);

        test(small_leds, "small (fast)", 0);
    }
    return 0;
}