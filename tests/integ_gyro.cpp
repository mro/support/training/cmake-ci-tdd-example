/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-05-02T14:36:23
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include <avr/io.h>
#include <math.h>
#include <stdio.h>

#include "TWI.hpp"
#include "system.hpp"
#include "twi/MPU6050.hpp"

static inline bool check(bool value)
{
    if (value)
        puts(": OK");
    else
        puts(": KO");
    return value;
}

static void clear_line(bool blank = false)
{
    for (size_t i = 0; i < 80; ++i)
        fwrite("\b", 1, 1, stdout);
    if (blank)
    {
        for (size_t i = 0; i < 80; ++i)
            fwrite(" ", 1, 1, stdout);
        clear_line(false);
    }
}

void test_angle(MPU6050 &gyro, bool pitch, float threshold)
{
    float value;
    printf("TEST: move %s in %s direction (%s80) to continue\n",
           pitch ? "pitch" : "roll", threshold > 0 ? "positive" : "negative",
           threshold > 0 ? ">" : "< -");
    do
    {
        gyro.update();
        clear_line();
        printf("pitch: %f roll: %f", gyro.pitch(), gyro.roll());
        fflush(stdout);

        value = pitch ? gyro.pitch() : gyro.roll();

    } while (threshold > 0 ? (value < threshold) : (value > threshold));
    clear_line();
    return;
}

int main()
{
    sys_init();
    DDRD &= ~(1 << PD2); // input
    PORTD |= (1 << PD2); // pull-up

    TWI twi;
    MPU6050 gyro(twi);

    gyro.init();

    while (1)
    {
        gyro.update();

        float pitch = gyro.pitch(), roll = gyro.roll();
        printf("TEST: initial position pitch: %f, roll: %f...", pitch, roll);
        check(fabs(pitch) <= 1.0 && fabs(roll) <= 1.0);

        test_angle(gyro, true, 80.0);
        test_angle(gyro, true, -80.0);
        test_angle(gyro, false, 80.0);
        test_angle(gyro, false, -80.0);

        puts("TEST: move pitch and roll back to zero");
        do
        {
            gyro.update();
            clear_line();
            printf("pitch: %f roll: %f", gyro.pitch(), gyro.roll());

        } while (fabs(gyro.pitch()) >= 5.0 || fabs(gyro.roll()) >= 5.0);
        clear_line(true);

        puts("OK");

        puts("");
        puts("Press button to (re)start test");
        fflush(stdout);

        while ((PIND & (1 << PD2)))
            delay_us(1);
    }
}