FROM debian:stable-slim

RUN apt-get update && apt-get dist-upgrade -y && \
    apt-get install -y gcc-avr avr-libc avrdude cmake git gdb gdb-avr simavr
RUN apt-get install -y build-essential clangd clang clang-format cppcheck clang-tidy \
    libcppunit-dev minicom cmake-curses-gui

RUN mkdir -p /etc/minicom && \
    echo "pu baudrate         9600\
pu bits             8\
pu parity           N\
pu stopbits         1" > /etc/minicom/minirc.plane

